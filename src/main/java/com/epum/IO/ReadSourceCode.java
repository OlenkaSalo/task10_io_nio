package com.epum.IO;

import java.io.*;
import java.util.Scanner;

public class ReadSourceCode {
     public  void readSC() throws IOException {
         Scanner sc = new Scanner(System.in);
         File file = new File(sc.next());
         BufferedReader bf = new BufferedReader(new FileReader(file));
         String input;

         try{
             while((input=bf.readLine())!=null)
             {
             System.out.println(input);
             }
         }catch (IOException e) {
             e.printStackTrace();
         }finally {
             bf.close();

         }


     }
}
