package com.epum.IO;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class DisplayContentOfDirectory {

    public void displayDirectory(File file) throws IOException
    {

        if(file.isDirectory())
        {
            for(File f: file.listFiles())
            {

               displayDirectory(f);
            }
        }else if(file.exists()){
            System.out.println(file.getPath());
        }else{
            throw new FileNotFoundException(file.getPath());
        }

    }
}
