package com.epum.NIO;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

public class BuffClass {

    public void buffChanel() throws IOException
    {
        FileInputStream fileIn = new FileInputStream("D:\\Helen!\\Java\\Thinking_inJava.txt");
        FileOutputStream fileOut = new FileOutputStream("D:\\Helen!\\Java\\Thinking_inJavaOut.txt");

        ReadableByteChannel sourceIn = fileIn.getChannel();
        WritableByteChannel sourceOut = fileOut.getChannel();

        recordData(sourceIn,sourceOut);
        System.out.println("File record in new file");

    }

    private static void recordData(ReadableByteChannel sourceIn, WritableByteChannel sourceOut) throws IOException {
        {
            ByteBuffer buff = ByteBuffer.allocateDirect(512);
            while(sourceIn.read(buff)!=-1)
            {
               buff.flip();

               while(buff.hasRemaining())
               {
                   sourceOut.write(buff);
               }
               buff.clear();
            }
        }
    }
}
