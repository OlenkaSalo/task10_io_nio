package com.epum.NIO;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class NIOServer {
    public static void main(String[] args) throws IOException {
        NIOServer nioS = new NIOServer();
        nioS.nioServer();
    }

    public void nioServer() throws IOException {
        try {
            Selector selector = Selector.open();
            InetAddress hostIP = InetAddress.getLocalHost();


            ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
            InetSocketAddress inetSocketAddress = new InetSocketAddress(hostIP, 199);

            serverSocketChannel.bind(inetSocketAddress);

            serverSocketChannel.configureBlocking(false);

            int ops = serverSocketChannel.validOps();
            SelectionKey selectionKey = serverSocketChannel.register(selector, ops, null);

            while (true) {
                System.out.println("Server is  waiting for connection" + " " + hostIP.getHostAddress() + ":" + 199);
                if (selector.select() > 0) {


                    Set<SelectionKey> selectionKeys = selector.selectedKeys();
                    Iterator<SelectionKey> selectionKeyIterator = selectionKeys.iterator();

                    while (selectionKeyIterator.hasNext()) {
                        SelectionKey key = selectionKeyIterator.next();

                        if (key.isAcceptable()) {
                            SocketChannel socketClient = serverSocketChannel.accept();

                            socketClient.configureBlocking(false);
                            socketClient.register(selector, SelectionKey.OP_READ);

                            System.out.println("Connection accept " + socketClient.getLocalAddress());

                        } else if (key.isReadable()) {
                            SocketChannel socketClient = (SocketChannel) key.channel();
                            ByteBuffer byteBuffer = ByteBuffer.allocate(512);
                            socketClient.read(byteBuffer);
                            String s = new String(byteBuffer.array()).trim();

                            System.out.println("EXECUTE!! " + s);

                            if (s.equals("Finally")) {
                                socketClient.close();
                            }
                        }
                        selectionKeyIterator.remove();
                    }
                }
            }
        }catch (IOException e)
        {
            e.printStackTrace();
        }
    }

}
