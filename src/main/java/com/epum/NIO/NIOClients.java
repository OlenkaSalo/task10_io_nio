package com.epum.NIO;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;

public class NIOClients {
    public static void main(String[] args) throws IOException, InterruptedException {
        NIOClients nioCl = new NIOClients();
        nioCl.nioClients();
    }

    public void nioClients() throws IOException, InterruptedException, UnknownHostException {
       try{
        InetAddress hostIP = InetAddress.getLocalHost();
        InetSocketAddress MyinetSocketAddress = new InetSocketAddress(hostIP, 199);
        SocketChannel channelClient = SocketChannel.open(MyinetSocketAddress);

        System.out.println("Connecting to Server..." + hostIP.getHostAddress()+":"+MyinetSocketAddress.getPort() );

       List<String> messages = new ArrayList<String>();

        messages.add("First message");
        messages.add("Second message");
        messages.add("Third message");
        messages.add("Fourth massege");
        messages.add("Finally");

        for (String s : messages) {

            ByteBuffer buff = ByteBuffer.allocate(512);
            buff.put(s.getBytes());
            buff.flip();
            int bWrite = channelClient.write(buff);
            System.out.println("Send message...:" + s + bWrite);

        }
            System.out.println("Close Client connection...");
channelClient.close();


        }catch (IOException e){
           System.out.print(e.getMessage());
           e.printStackTrace();
       }

    }
    }

