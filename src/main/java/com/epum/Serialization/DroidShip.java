package com.epum.Serialization;

import java.io.Serializable;

public class DroidShip implements Serializable {

    transient String mission="Destroy all planet!!!";
    final transient String capName = "Droiiii";
    private int droids;

    public  DroidShip(int droids)
    {
        this.droids=droids;
    }

    public int getDroids()
    {
        return droids;
    }

}
